# Contributing to Kubernetes Monitoring Helm Chart

Thank you for your interest in contributing to the Kubernetes Monitoring Helm Chart project, maintained by the CERN IT Monitoring team. This Helm chart facilitates the collection of both metrics and logs from Kubernetes clusters and the applications running on them. We appreciate your efforts to improve and maintain the chart!

Below are the guidelines to help you contribute effectively.

## How to Contribute

### 1. Fork the Repository
Begin by creating a fork of the main repository hosted on GitLab at:

`gitlab.cern.ch/monitoring/helm-charts/kubernetes-monitoring`

Forking the repository allows you to freely experiment with changes without affecting the original repository.

### 2. Create a Branch for Your Changes
Once you've forked the repository, create a feature branch from the `master` branch. A good practice is to name the branch based on the feature or fix you're working on, e.g., `feature/log-enhancement` or `fix/documentation-typo`.

```bash
git checkout -b feature/my-awesome-feature
```

### 3. Implement and Test Your Changes
Develop your changes locally. Before submitting your contribution, please ensure that you test your changes thoroughly by running the chart locally:

```bash
helm install . cern-it-monitoring-kubernetes -f values.yaml -f my-values.yaml
```
This ensures that your changes do not introduce any unintended issues.

### 4. Update Documentation
If your changes modify or extend the functionality of the chart, do not forget to update the relevant documentation (such as `README.md` or any related configuration files) to reflect these modifications.

### 5. Submit a Merge Request
Once your changes are ready, push your branch to your fork and create a Merge Request (MR) targeting the `master` branch of the main repository.

In your MR, provide a clear and detailed explanation of your changes. Include the motivation behind the change, a description of what was modified, and any potential impact or known drawbacks. Make sure to tag the appropriate reviewers from the CERN IT Monitoring team for review.