#!/bin/bash

# Check if the file argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <requirements_file>"
    exit 1
fi

# Path to the requirements file (first argument)
requirements_file="$1"

# Base destination registry
destination_registry="docker://registry.cern.ch/monit"

# Loop through each line in the requirements file
while IFS= read -r line; do
    # Extract image name and version
    origin_image="docker://$line"

    # Extract the base image name for the destination (without the registry)
    image_name=$(echo "$line" | cut -d'/' -f3)

    # Construct the destination image
    destination_image="$destination_registry/cern-it-monitoring-$image_name"

    # Run the skopeo copy command
    echo "Moving [$origin_image] to [$destination_image]."
    skopeo copy "$origin_image" "$destination_image" \
        --override-os linux \
        --override-arch amd64 \
        --dest-creds "$REGISTRY_USER:$REGISTRY_PASSWORD"
    
done < "$requirements_file"
