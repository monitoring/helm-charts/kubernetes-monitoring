.PHONY: fetch_crds all

# https://github.com/prometheus-community/helm-charts/releases
VERSION ?= 13.0.2

CRDS_PATH = charts/crds/crds
URL = https://github.com/prometheus-community/helm-charts/releases/download/prometheus-operator-crds-$(VERSION)/prometheus-operator-crds-$(VERSION).tgz

fetch_crds:
	rm -rf ${CRDS_PATH}/*.yaml
	curl -s -L -o - ${URL} | tar -C ${CRDS_PATH} --strip-components=4 -xzf - prometheus-operator-crds/charts/crds/templates
	find ${CRDS_PATH} -type f -name '*.yaml' -exec sed -i '/{{- with .Values.annotations }}/d' {} +
	find ${CRDS_PATH} -type f -name '*.yaml' -exec sed -i '/{{- toYaml . | nindent 4 }}/d' {} +
	find ${CRDS_PATH} -type f -name '*.yaml' -exec sed -i '/{{- end }}/d' {} +
	sed -i '/^  annotations:/a \    argocd.argoproj.io/sync-options: ServerSideApply=true' ${CRDS_PATH}/*.yaml

all: fetch_crds
