# CERN IT Monitoring Kubernetes Helm Chart Default Values
This file contains the markdown version of the default values that this chart takes.

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| kubernetes.clusterName | string | - | name of the kubernetes cluster to monitor. This value will be appended to every metric and log via k8s_cluster_name label |
| logs.enabled | bool | `false` | indicates if logs components should be enabled or not. If set to false no logs component will be installed nor configured |
| logs.fluentbit.customParsers | string | `""` |  |
| logs.fluentbit.enabled | bool | `false` | indicates if fluentbit logs component should be installed or not |
| logs.fluentbit.extraVolumeMounts | list | `[]` |  |
| logs.fluentbit.extraVolumes | list | `[]` |  |
| logs.fluentbit.filters | string | Kubernetes filter. See `values.yaml` file. | fluentbit filters as a yaml list in a multiline string |
| logs.fluentbit.inputs | string | Tail plugin over `/var/log/containers/*.log` files. See `values.yaml` file. |
| logs.fluentbit.outputs | string | OpenTelemetry plugin using `otlp.endpoint`, `otlp.port`, `tenant.username` and `tenant.password`. See `values.yaml`. | fluentbit outputs as a yaml list in a multiline string |
| logs.fluentbit.resources.limits.cpu | string | `"20m"` |  |
| logs.fluentbit.resources.limits.memory | string | `"25Mi"` |  |
| logs.fluentbit.resources.requests.cpu | string | `"5m"` |  |
| logs.fluentbit.resources.requests.memory | string | `"15Mi"` |  |
| logs.fluentbit.scrapeInterval | string | `"15s"` | interval used by the local prometheus (if installed) to scrape metrics from logs fluentbits |
| logs.fluentbit.service | string | Daemon mode off listening on port 2020. See `values.yaml`. | fluentbit service configuration options in a multiline string |
| metrics.enabled | bool | `true` | indicates if all metrics components should be enabled or not. If set to false no metrics component will be installed nor configured |
| metrics.defaultNodeSelector | map | `{}` | if set will be used as `nodeSelector` for those components that allow one |
| metrics.fluentbit.enabled | bool | `true` | if true fluentbit will be installed |
| metrics.fluentbit.diskMaxCache | string | `5G` | max size for in-disk storage for fluent-bit |
| metrics.fluentbit.nodeSelector | hash | `"nil"` | fluentbit statefulset node selectors |
| metrics.fluentbit.extraVolumeMounts | list | `[]` |  |
| metrics.fluentbit.extraVolumes | list | `[]` |  |
| metrics.fluentbit.filters | string | `"nil"` | fluentbit filters as a yaml list in a multiline string |
| metrics.fluentbit.inputs | string | Configuration to scrape local prometheus. See `values.yaml`. | fluentbit inputs as a yaml list in a multiline string |
| metrics.fluentbit.matchQuery | string | `"match[]={job!=\"\"}"` | Query parameter to apply to the federate Prometheus URL, use this to filter and send only specific metrics |
| metrics.fluentbit.prometheusScrapeBufferMaxSize | string | `"100M"` | fluentbit buffer size. The more metrics to send the bigger needs to be |
| metrics.fluentbit.prometheusScrapeInterval | string | `"60s"` | interval used by fluentbit to scrape metrics from prometheus |
| metrics.fluentbit.resources.limits.cpu | string | `"1"` |  |
| metrics.fluentbit.resources.limits.memory | string | `"1Gi"` |  |
| metrics.fluentbit.resources.requests.cpu | string | `"1"` |  |
| metrics.fluentbit.resources.requests.memory | string | `"150Mi"` |  |
| metrics.fluentbit.service | string | Daemon mode off listening on port 2020. See `values.yaml`. | fluentbit service configuration options in a multiline string |
| metrics.kubeState.enabled | bool | `true` | if true kube state will be installed together with a service monitor |
| metrics.kubeState.nodeSelector | hash | `"nil"` | kubeState deployment node selectors |
| metrics.kubeState.resources.limits.cpu | string | `"20m"` |  |
| metrics.kubeState.resources.limits.memory | string | `"25Mi"` |  |
| metrics.kubeState.resources.requests.cpu | string | `"5m"` |  |
| metrics.kubeState.resources.requests.memory | string | `"15Mi"` |  |
| metrics.kubeState.scrapeInterval | string | `"15s"` | indicates how often kube state will be scraped by the local prometheus |
| metrics.metricsserver.enabled | bool | `true` | if true metrics server will be installed |
| metrics.metricsserver.nodeSelector | hash | `"nil"` | metricsserver node selectors |
| metrics.metricsserver.resources.limits.cpu | string | `"100m"` |  |
| metrics.metricsserver.resources.limits.memory | string | `"200Mi"` |  |
| metrics.metricsserver.resources.requests.cpu | string | `"100m"` |  |
| metrics.metricsserver.resources.requests.memory | string | `"200Mi"` |  |
| metrics.nodeExporter.enabled | bool | `true` | if true node exporter will be installed as a daemon set together with a pod monitor |
| metrics.nodeExporter.resources.limits.cpu | string | `"20m"` |  |
| metrics.nodeExporter.resources.limits.memory | string | `"25Mi"` |  |
| metrics.nodeExporter.resources.requests.cpu | string | `"5m"` |  |
| metrics.nodeExporter.resources.requests.memory | string | `"15Mi"` |  |
| metrics.nodeExporter.scrapeInterval | string | `"15s"` | indicates how often node exporter will be scraped by the local prometheus |
| metrics.prometheus.enabled | bool | `true` | if true prometheus operator and a prometheus server will be installed |
| metrics.prometheus.operator | object | Resources configuration. See `values.yaml`. | specific configuration for the prometheus operator |
| metrics.prometheus.operator.nodeSelector | hash | `"nil"` | prometheus operator node selectors |
| metrics.prometheus.server.nodeSelector | hash | `"nil"` | prometheus server node selectors |
| metrics.prometheus.server.extraLabelsForMetrics | hash | `{}` | set of static labels and values to add to all the metrics gathered by the in-cluster prometheus when exported to central monitoring |
| metrics.prometheus.server.remoteWrite | object | `{}` | remote write prometheus configuration |
| metrics.prometheus.server.resources.limits.cpu | string | `"500m"` |  |
| metrics.prometheus.server.resources.limits.memory | string | `"5Gi"` |  |
| metrics.prometheus.server.resources.requests.cpu | string | `"100m"` |  |
| metrics.prometheus.server.resources.requests.memory | string | `"2Gi"` |  |
| metrics.prometheus.server.retention | string | `"24h"` | interval during which local cluster prometheus will store metrics |
| metrics.prometheus.server.scrapeInterval | string | `"10s"` | interval used to self scrape metrics |
| metrics.prometheus.server.scrapeTimeout | string | `"5s"` | timeout for self scraped metrics |
| metrics.prometheus.server.image | string | `"registry.cern.ch/monit/cern-it-monitoring-prometheus:v2.50.0"` | prometheus image to use by the local cluster prometheus |
| metrics.prometheus.server.version | string | `"v2.50.0"` | prometheus version to use by the local cluster prometheus |
| metrics.alertmanager.enabled | bool | `false` | if true alertmanager will be installed and prometheus reconfigured to use it as the alerting endpoint |
| metrics.alertmanager.image | string | `"registry.cern.ch/monit/cern-it-monitoring-alertmanager"` | alertmanager image to use by the local cluster alertmanager |
| metrics.alertmanager.tag | string | `"v0.27.0"` | alertmanager image tag to be used when pulling it |
| metrics.alertmanager.pullPolicy | string | `"IfNotPresent"` | pull policy for the alertmanager image |
| metrics.alertmanager.replicas | int | `3` | number of replicas for the alertmanager deployment, defaults 3 for HA |
| metrics.alertmanager.ingress.enabled | bool | `false` | if set to true an ingress will be created for the alertmanager service |
| metrics.alertmanager.ingress.className | string | `""` | class name to be used by the alertmanager ingress |
| metrics.alertmanager.ingress.path | string | `"/"` | entry path for the alertmanager ingress |
| metrics.alertmanager.ingress.pathType | string | `"ImplementationSpecific"` | path type for the alertmanager ingress |
| metrics.alertmanager.ingress.hosts | Array | `[]` | list of hosts for the alertmanager ingress |
| metrics.alertmanager.ingress.tls | Hash | `{}` | tls configuration for the alertmanager ingress |
| metrics.alertmanager.nodeSelector | Hash | `{}` | node selector configuration for the alertmanager |
| metrics.alertmanager.volumes | Array | `[]` | list of volumes to be declared |
| metrics.alertmanager.volumeMounts | Array | `[]` | list of volumes to be mounted |
| otlp.endpoint | string | `"monit-otlp.cern.ch"` | otlp endpoint where the otlp receivers are listening |
| otlp.port | int | `4319` | otlp port where the otlp receivers are listening |
| tenant.name | string | - | username used for authenticating with the MONIT infrastructure |
| tenant.password | string | - | password (plain) used for authenticating with the MONIT infrastructure |
| crds.enabled | bool | `true` | whether to install Prometheus operator's CRDs |

----------------------------------------------
