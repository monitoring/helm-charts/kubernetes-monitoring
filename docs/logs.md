# Logs Configuration Guide

This guide provides instructions on configuring and managing logs collection in the **CERN IT Monitoring Kubernetes Helm Chart**. Logs provide valuable insights into the operation and health of both the Kubernetes cluster and the applications running within it. This chart uses **Fluentbit** to gather and forward logs to the central monitoring infrastructure.

## 1. Enabling/Disabling Logs

The logging system is controlled via the `logs.enabled` flag. By default, logs collection is disabled.

To **enable** or **disable** logs collection:

```yaml
logs:
  enabled: true  # Set to 'false' to disable logs collection
```
If `logs.enabled` is set to `false`, no logging components (such as Fluentbit) will be installed or configured.

## 2. Base Logs Collected

By default, the chart deploys **Fluentbit** as a DaemonSet to gather logs from Kubernetes containers. Fluentbit is highly efficient and lightweight but can generate substantial API requests in large deployments (100+ nodes) due to the Kubernetes filter.

### Fluentbit Configuration
Fluentbit collects logs from the `/var/log/containers/` directory and applies a set of filters before forwarding them to the central monitoring system via the OpenTelemetry protocol.

```yaml
logs:
  fluentbit:
    enabled: true  # Enable/disable Fluentbit for log collection
    scrapeInterval: "15s"  # Prometheus scrape interval for Fluentbit metrics
    resources:
      requests:
        cpu: "5m"
        memory: "15Mi"
      limits:
        cpu: "20m"
        memory: "25Mi"
```

The **Fluentbit DaemonSet** is deployed on all nodes, scraping container logs and processing them based on the inputs, filters, and outputs defined in the configuration.

### Fluentbit Service Configuration
The Fluentbit service is configured with the following options:

```yaml
service: |
  [SERVICE]
      Daemon Off
      Flush 1
      Log_Level INFO
      HTTP_Server On
      HTTP_Listen 0.0.0.0
      HTTP_Port 2020
      Health_Check On
```

### Fluentbit Input Configuration
By default, Fluentbit gathers logs from all containers under `/var/log/containers/*`.log and uses the **CRI parser** to handle multiline log entries.

```yaml
inputs: |
  [INPUT]
      Name tail
      Path /var/log/containers/*.log
      multiline.parser cri
      Tag kube.*
      Mem_Buf_Limit 20MB
      Skip_Long_Lines Off
```

### Fluentbit Filter Configuration
Fluentbit applies several filters to enrich logs with Kubernetes metadata (e.g., labels and annotations), and organizes the structure of the log records.

```yaml
filters: |
  [FILTER]
      Name                kubernetes
      Match               kube.*
      Kube_URL            https://kubernetes.default.svc:443
      Kube_CA_File        /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      Kube_Token_File     /var/run/secrets/kubernetes.io/serviceaccount/token
      Merge_Log           On
      K8S-Logging.Parser  On
      K8S-Logging.Exclude Off
```

### Fluentbit Output Configuration
Logs are forwarded using the **OpenTelemetry** protocol to the specified endpoint, adding custom labels (such as cluster name) to the logs.

```yaml
outputs: |
  [OUTPUT]
      name opentelemetry
      match *
      add_label job kubernetes
      add_label k8s_cluster_name {{ .Values.kubernetes.clusterName }}
      host {{ .Values.otlp.endpoint }}
      port {{ .Values.otlp.port }}
      logs_uri /v1/logs
      tls on
      tls.verify off
      http_user {{ .Values.tenant.name }}
      http_passwd {{ .Values.tenant.password }}
```

### Fluentbit Custom Lua Scripts
In Fluent Bit we can use Lua scripts to create processors that transform the records. These scripts are available in the Fluent Bit container under the path `/fluent-bit/etc/scripts`. In order to create new Lua scripts just add them in the values `logs.fluentbit.luaScripts` key.

For example, the following configuration will create two files in `/fluent-bit/etc/scripts`. One named `my_lua_script.lua` and another one named `my_lua_script2.lua`.
```yaml
luaScripts:
  my_lua_script.lua: |
    function my_function(tag, timestamp, record)
      // Do something...
      return 2, timestamp, record
      end
  my_other_lua_script.lua: |
    ...
```

## 3. Customizing Fluentbit for Additional Log Sources

You can extend the logging configuration by extending previous configurations with custom Fluentbit **inputs**, **filters**, or **outputs**. This is especially useful if you want to gather logs from additional sources or apply different processing to specific logs.

### Extra Volumes and Volume Mounts
You can also configure **extra volumes** and **volume mounts** for Fluentbit to collect logs from different paths or persistent volumes:

```yaml
logs:
  fluentbit:
    extraVolumes:
      - name: extra-logs
        hostPath:
          path: /mnt/logs
    extraVolumeMounts:
      - name: extra-logs
        mountPath: /mnt/logs
```

## 4. Considerations for Large Deployments

Be cautious when enabling Fluentbit for large clusters (100+ nodes). The **Kubernetes filter** generates API requests to the Kubernetes API server, and these requests may become significant in large-scale deployments. In such cases, you may need to:

- Adjust Fluentbit resource requests and limits.
- Tune the API request rate by modifying the filters.
- Consider other optimization techniques, such as excluding specific log sources.