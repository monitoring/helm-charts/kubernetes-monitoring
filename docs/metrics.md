# Metrics Configuration Guide

This guide covers how to configure and manage metrics collection in the **CERN IT Monitoring Kubernetes Helm Chart**. Metrics provide critical insights into your cluster's health, performance, and resource utilization. The chart uses the **Prometheus Operator** to collect and forward these metrics.


## 1. Enabling/Disabling Metrics

Metrics collection is managed by the `metrics.enabled` flag in the Helm chart configuration. By default, metrics collection is enabled.

To **enable** or **disable** metrics collection:

```yaml
metrics:
  enabled: true  # Set to 'false' to disable all metrics components
```


## 2. Base Metrics Collected
By default, the chart monitors a set of key metrics from both Kubernetes and the nodes in your cluster. Below are the main components for base metrics collection:

### A. Node Exporter
The Node Exporter collects essential resource metrics from the nodes, including CPU, memory, and network usage.

### B. Kube State Metrics
The Kube State Metrics exporter collects data from the Kubernetes API regarding resource usage, limits, and object states within the cluster.

### C. Prometheus Operator
The Prometheus Operator is deployed to scrape and manage metrics collection from the cluster and the applications that run on it.
This allows you to define custom resources like `podmonitors` or `servicemonitors` and forward those metrics to the monitoring infrastructure. More information about the prometheus operator [here](https://prometheus-operator.dev/docs/api-reference/api/).

Another option is to directly add the `servicemonitors` directly in the helm chart values:

```yaml
metrics:
  prometheus:
    server:
      serviceMonitors:
        - name: nginx-service-monitor
          spec:
            endpoints:
              - interval: 30s
                port: metrics  # The port where the NGINX metrics are exposed
            namespaceSelector:
              matchNames:
                - default  # Namespace where NGINX is running
            selector:
              matchLabels:
                app: nginx
```

### D. Push Gateway Metrics
Sometimes you cannot scrape your metrics from an endpoint using a ServiceMonitor, PodMonitor or other solutions. For this scenario we have added the option to deploy a local Prometheus Push Gateway fully configured to be scraped from the local Prometheus and send the metrics to the IT Monitoring infrastructure. To configure it you have the following options:

```yaml
metrics:
  pushgateway:
    enabled: false
    image:
      repository: registry.cern.ch/monit/cern-it-monitoring-pushgateway
      tag: v1.10.0
      pullPolicy: IfNotPresent
    resources:
      requests:
        cpu: 0.2
        memory: 100Mi
      limits:
        cpu: 0.2
        memory: 100Mi
    # If set to true will install register a new ingress with the given
    # configuration.
    ingress:
      enabled: false
      className: "" # If no class set then default.
      path: /
      pathType: ImplementationSpecific
      hosts: [] # A list of hosts.
      tls: {} # Kubernetes plain ingress tls config.
    # If given will override the defaultNodeSelector and install the component
    # only on the nodes that match the given condition.
    nodeSelector: {}
```

This configuration will create a service in your cluster named `it-monit-metrics-collector-pushgateway`. In this service there is a port named `http` (9091) where you can push your metrics. If you need to push metrics from outside the cluster you only need to enable the ingress and point the corresponding dns records to your cluster ingress nodes.

### E. Metrics Drop and Transformations
With the recent updates, it is now possible to configure user-provided relabelings for various Kubernetes components monitored by Prometheus. This enhancement provides greater flexibility in transforming, dropping, or modifying metrics based on user-defined criteria.

All the components can be configured as follows, applied under its corresponding key:

```yaml
relabelings:
  - action: drop
    sourceLabels: [__name__]
    regex: ".*_unwanted_metric"
  - action: drop
    sourceLabels: [__name__]
    regex: ".*_unwanted_metric_two"
```

Here is the list of components and the YAML key that corresponds to it:
  - api server: `metrics.apiServer.serviceMonitor.relabelings`
  - coredns: `metrics.coredns.serviceMonitor.relabelings`
  - ectd: `metrics.etcd.serviceMonitor.relabelings`
  - ingress nginx: `metrics.ingress.nginx.serviceMonitor.relabelings`
  - kube state: `metrics.kubeState.serviceMonitor.relabelings`
  - kube controller: `metrics.kubeController.serviceMonitor.relabelings`
  - kubelet: `metrics.kubelet.serviceMonitor.relabelings`
  - kubeproxy: `metrics.kubeProxy.serviceMonitor.relabelings`
  - node exporter: `metrics.nodeExporter.serviceMonitor.relabelings`
  - scheduler: `metrics.kubeScheduler.serviceMonitor.relabelings`

Also, there is a general option that allows you to apply the relabeling option generally. The difference is that with previous options Prometheus will not store locally the metrics (saving memory), but user will need to know which component creates which metric. With the alternate method Prometheus will store the metrics but not forward them. In this case users do not need to know from where the metric is gathered.

```yaml
metrics:
  prometheus:
    server:
      relabelings:
        - action: drop
          sourceLabels: [__name__]
          regex: ".*_unwanted_metric"
```

## 4. Pushing Metrics to Central Monitoring Infrastructure
By default the Helm chart uses a dedicated Fluent Bit to upload the metrics to the Central Monitoring Infrastructure via OpenTelemetry protocol.

This Fluent Bit already has a default configuration for the inputs, filters and outputs. Please, check the `values.yaml` to see the default configuration.

Besides these basic configuration options, others are also offered.

### Fluentbit Custom Lua Scripts
In Fluent Bit we can use Lua scripts to create processors that transform the records. These scripts are available in the Fluent Bit container under the path `/fluent-bit/etc/scripts`. In order to create new Lua scripts just add them in the values `logs.fluentbit.luaScripts` key.

For example, the following configuration will create two files in `/fluent-bit/etc/scripts`. One named `my_lua_script.lua` and another one named `my_lua_script2.lua`.
```yaml
luaScripts:
  my_lua_script.lua: |
    function my_function(tag, timestamp, record)
      // Do something...
      return 2, timestamp, record
      end
  my_other_lua_script.lua: |
    ...
```


## 5. Further Customizations

You can further customize metrics collection with the following settings:

* **Node Selector**: Apply specific node selectors to metrics components like Prometheus, Node Exporter, and Kube State Metrics.
* **Resource Requests & Limits**: Fine-tune resource requests and limits for Prometheus, exporters, and forwarders to optimize performance.
* **Remote Write Configuration**: Configure Prometheus to write metrics remotely to your own prometheus instance.
