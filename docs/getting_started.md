# Getting Started with CERN IT Monitoring Kubernetes Helm Chart

This guide will help you quickly deploy the **CERN IT Monitoring Kubernetes Helm Chart** to collect metrics and logs from your Kubernetes cluster and forward them to CERN's centralized monitoring infrastructure.


## Prerequisites

Before you begin, ensure you have the following:

- **Kubernetes Cluster**: A running Kubernetes cluster (v1.27 or higher is required).
- **Helm**: Helm v3 or higher installed on your local machine. Follow the [Helm installation guide](https://helm.sh/docs/intro/install/) if needed.
- **Access to CERN Monitoring Infrastructure**: Ensure your Kubernetes cluster has connectivity to the CERN central monitoring system. (By default this is the case except on special environments)


## 1. Install the CERN IT Monitoring Helm Repository

First, install the official **CERN IT Monitoring** Helm chart repository from your Helm client. Remember to substitute the `<gitlab-tag>` with the version you want to install (ex. `1.0.0-rc1`).
We recommend installing the chart in the `monitoring` namespace. If it does not exists you can create it.

```bash
helm install cern-it-monitoring-kubernetes oci://registry.cern.ch/monit/cern-it-monitoring-kubernetes --version <gitlab-tag> -f my-values.yaml -n monitoring
```
You can customize it by passing values from your `values.yaml` or using custom flags. Remember that you will need to pass at least the following values via the file `values.yaml` in order to send data to the central monitoring infrastructure (this is valid for both metrics and logs):

```yaml
tenant:
  name: nil # Your tenant.
  password: nil # Your password.

kubernetes:
  clusterName: nil # The name of your cluster.
```
If you are missing any of the previous value do not hesitate to open a [SNOW ticket](cern.ch/monit-support) to the monitoring team.

## 2. Customize Chart Configuration

You may want to customize the chart’s configuration to better suit your environment. Key configurations include:

* Metrics Collection: Set up how metrics are gathered from your Kubernetes components and applications.
* Logs Collection: Configure the forwarding of logs to the central monitoring infrastructure.

For a full list of configurable options, refer to the [values.yaml](values.md).


## 3. Verify the Installation

After the Helm chart is installed, verify that the services and pods are running:

```bash
kubectl get pods -n monitoring
```

Check if logs and metrics are being correctly forwarded to CERN's central monitoring infrastructure. For that you can explore the logs of the Fluent Bit pods that forward the metrics to the central monitoring infrastructure. You can use:

```bash
kubectl logs prometheus-it-monit-metrics-collector-prometheus-0 -n monitoring
kubectl logs it-monit-metrics-collector-fluentbit-0 -n monitoring
```


## 4. Updating the Chart

You might need to update the existing configuration of your chart. For that the easiest solution will be to apply again a set of values. Foe example:

```bash
# 1. Get the existing values
helm get values cern-it-monitoring-kubernetes > existing-values.yaml -n monitoring;
# 2. Modify the values as you need.
# 3. Apply them again to the same version.
helm upgrade cern-it-monitoring-kubernetes oci://registry.cern.ch/monit/cern-it-monitoring-kubernetes --version <gitlab-tag> -f existing-values.yaml -n monitoring
```

To upgrade your Helm release with new features or fixes, simply run:

```bash
helm upgrade cern-it-monitoring-kubernetes oci://registry.cern.ch/monit/cern-it-monitoring-kubernetes --version <new-gitlab-tag> -f my-existing-values.yaml -n monitoring
```
Ensure that the `my-existing-values.yaml` file is updated as per your needs before upgrading.


## 5. Uninstalling the Chart

To remove the monitoring setup from your Kubernetes cluster, use the following command:

```bash
helm uninstall cern-it-monitoring-kubernetes
```
This will remove most of the associated monitoring components deployed by this Helm chart. The CRDs will be left behind, though.


## Additional Resources

* Helm client documentation: [https://helm.sh](https://helm.sh).
* Helm working with OCI registries: [https://helm.sh/docs/topics/registries/](https://helm.sh/docs/topics/registries/)
