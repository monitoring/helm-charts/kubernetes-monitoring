# CERN IT Monitoring Kubernetes Helm Chart

## Overview

The **CERN IT Monitoring Kubernetes Helm Chart** provides a solution for monitoring Kubernetes clusters at CERN. It enables the collection of **metrics**, **logs**, and future support for **traces**, which are forwarded to the central CERN monitoring infrastructure. From there users can consume them using the day-to-day tools that they already user like [Grafana](https://monit-docs.web.cern.ch/access/grafana/) or [OpenSearch](https://monit-docs.web.cern.ch/access/opensearch/).

This Helm chart simplifies the deployment and configuration of necessary components for observability, making it easier to manage monitoring across various Kubernetes clusters and their applications.

## Quick Start

To get started with deploying the chart, follow the detailed installation instructions available in our [documentation](https://monit-docs.web.cern.ch).

```bash
helm install cern-it-monitoring-kubernetes oci://registry.cern.ch/monit/cern-it-monitoring-kubernetes --version <gitlab-tag>
```
For detailed configuration options and values, refer to the [values.yaml](values.yaml) file and the [documentation page](https://monit-docs.web.cern.ch).

## Contributing

We welcome contributions! If you're interested in helping improve this project, please review our [contribution guidelines](CONTRIBUTING.md). In brief:

1. **Fork** the repository.
2. Create a **feature branch**.
3. Implement and **test** your changes.
4. Submit a **Merge Request (MR)** to the `master` branch.

For a full contribution workflow, visit the [contribution guide](CONTRIBUTING.md).

## Documentation

Complete documentation for this chart, including setup and configuration details, is available:

- Project Documentation: [link](https://monit-docs.web.cern.ch)
- GitLab Repository: [link](docs)
- License: Apache-2.0. See the [LICENSE](LICENSE) file for details.

## License

This repository is licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0). See the [LICENSE](LICENSE) file for more information.

---
For more details, visit the [CERN Monitoring Documentation](https://monit-docs.web.cern.ch) or explore the [docs](docs) folder.